import React, {createContext} from 'react'

import ListMobiles from'../components/listMobiles/ListMobiles'
import MobilDetail from'../components/mobilDetail/MobilDetail'

export const API_ALL_PRODUCT = 'https://front-test-api.herokuapp.com/api/product'
export const API_PUT_PRODUCT = 'https://front-test-api.herokuapp.com/api/cart'
export const API_GET_PRODUCT = 'https://front-test-api.herokuapp.com/api/product/'

export const routes = [
	{ path: "/", name: "Home", element: <ListMobiles /> },
    { path: "/mobile/:mobileId", name: "View mobile", element: <MobilDetail /> }
]

export const ERROR_NO_SERVICE = 'The service is unavailable for now, please try again later.'
export const MESSAGE_OK = 'Product added successfully'

export const CartContext = createContext([]);
