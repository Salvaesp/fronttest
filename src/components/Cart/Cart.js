import React, {useState, useContext} from 'react';
import {CartContext} from '../../utils/constants.js'

import  './Cart.css'

const Cart = () =>{

    const {cart} = useContext(CartContext);
    const [tooltip, setTooltip] = useState(false)

    const prepareTooltip = () => {
        return cart.map((mobile) =>
            <div className='cartTooltip_item'>
                <img src={mobile.imgUrl} alt='imgCart' className='cartTooltip_itemImg' />
                <div className='cartTooltip_itemBrand'>{mobile.brand}</div>
                <div className='cartTooltip_itemModel'>{mobile.model}</div>
            </div>
        )
    }

    return (
        <div className='cart'>
            <div className='cart_container' onMouseEnter={()=>setTooltip(true)} onMouseOut={()=>setTooltip(false)}>item(s): {cart.length}</div>
            {cart.length ? <div className={tooltip ? 'cart_tooltip' : 'cart_tooltip cart_tooltip-hidden'}>{prepareTooltip()}</div> : ''}
        </div>
    )
}

export default Cart;
