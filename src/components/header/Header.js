import React from 'react'
import {Link} from "react-router-dom";
import Cart from '../Cart/Cart.js'
import BreadCrumbs from '../breadcrumbs/BreadCrumbs'

import  './Header.css'

const Header = () =>{
    return (
        <div className='header'>
            <div className='header_container'>
                <h1 className='header_title'><Link to='/'>The Mobile Store</Link></h1>
                <div className='header_cart'><Cart /></div>
            </div>
            <div className='header_breadcrumb'>
               <BreadCrumbs />
            </div>
        </div>
    )
}

export default Header;
