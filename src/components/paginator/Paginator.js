import React, {useState, useEffect} from 'react';
import  './Paginator.css'

const Paginator = ({init, maxView, length, click}) => {

    const [items, setItems] = useState([])

    useEffect(() => {
        const handlePaginator = () => {
            let itemsAux = []
            for (let i = 0; i < (length / maxView); i++) {
                let ini = i
                itemsAux.push(<span title={init} onClick={()=>click(ini)} className={init !== i ? 'paginator_item' : 'paginator_item paginator_item-selected'}>{i+1} | </span>)
            }
            setItems(itemsAux)
        }
        handlePaginator()
      }, [init, maxView, length, click]);

    return (
        <div className='paginator'>Paginator: {items}</div>
    )
}

export default Paginator;
