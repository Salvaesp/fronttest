import React, {useState, useEffect,useContext} from 'react';
import { useParams } from "react-router-dom"
import {API_PUT_PRODUCT, API_GET_PRODUCT, ERROR_NO_SERVICE, MESSAGE_OK, CartContext} from '../../utils/constants.js'

import './MobilDetail.css'

const MobilDetail = () => {

    const {mobileId} = useParams()

    const [showAllSpecs, setShowAllSpecs] = useState(true)

    const [color, setColor] = useState('-1')
    const [storage, setStorage] = useState('-1')
    const [error, setError] = useState('')
    const [errorService, setErrorService] = useState('')
    const [messageOK, setMessageOK] = useState('')

    const {addToCart} = useContext(CartContext);

    const [description, setDescription] = useState(undefined)

    const hadleDescription = (descriptions) => {
        let lis = []

        Object.keys(descriptions).forEach(function(key) {
            if (!Array.isArray(descriptions[key])) {
                lis.push(<li>{key + ': ' + descriptions[key]}</li>);
            } else{
                let subitems = descriptions[key].map((item) =>
                    <li>{item}</li>
                )
                lis.push(<ul>{subitems}</ul>)
            }
        });
        return lis
    }

    const handleSelect = (options) => {
        let opciones = []
        if (options.length > 0) {
            if (options.length > 1) {
                opciones.push(<option selected value='-1'>Select an option</option>)
            }
            options.map(({code, name}) =>
                opciones.push(<option value={code}>{name}</option>)
            )
        }

        return opciones
    }
    const changeSelect = (e, parameter) => {
        if (parameter === 'color') {
            setColor(e.target.value)
        } else {
            setStorage(e.target.value)
        }
    }

    useEffect(() => {
        fetch(API_GET_PRODUCT + mobileId)
        .then(res => res.json())
        .then(
            (result) => {
                if (result !== null && result !== undefined
                    && result.imgUrl !== null && result.imgUrl !== undefined
                    && result.model !== null && result.model !== undefined
                    && result.brand !== null && result.brand !== undefined) {
                        setDescription(result)
                        if (result.options.colors.length === 1 ) {
                            setColor(result.options.colors[0])
                        }
                        if (result.options.storages.length === 1 ) {
                            setStorage(result.options.storages[0])
                        }
                    }
                    else {
                        setErrorService(ERROR_NO_SERVICE)
                    }

            },
            (errord) => {
                console.log(errord)
                setErrorService(ERROR_NO_SERVICE)
            }
        )
    }, [mobileId]);

    const clickAddToCart = (id) => {
        if (color === '-1') {
            setError('Select a color')
            return false
        } else if (storage === '-1') {
            setError('Select a storage')
            return false
        } else {
            fetch(API_PUT_PRODUCT, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: id,
                    colorCode: 1,
                    storageCode: 2
                })
             })
             .then(res => res.json())
            .then((result) => {
                if (result !== null && result !== undefined && result.count !== null && result.count !== undefined) {
                    addToCart(description)
                    setMessageOK(MESSAGE_OK)
                    setTimeout(function(){
                        setMessageOK(null)
                    }, 5000);
                } else {
                    setErrorService(ERROR_NO_SERVICE)
                }
            },
            (errord) => {
                console.log(errord)
                setErrorService(ERROR_NO_SERVICE)
             }
            )
        }
    }

    const renderDetails = (descripcion) => {
        if (descripcion !== null && descripcion !== undefined) {
            return (
                <div>
                    <div className='mobilDetail_imgShop'>
                        <img src={descripcion.imgUrl} alt={descripcion.model} />
                        {error !== '' ? <div className='mobilDetail_error'>{error}</div> : ''}
                        <div className='mobilDetail_imgShopSelect'>Color:<select onChange={(e) => changeSelect(e, 'color')}>{handleSelect(descripcion.options.colors)}</select></div>
                        <div className='mobilDetail_imgShopSelect'>Storage:<select onChange={(e) => changeSelect(e, 'storage')}>{handleSelect(descripcion.options.storages)}</select></div>
                        <button className='mobilDetail_imgShopButtonCart' disabled={(color === '-1' || storage === '-1')} onClick={()=>clickAddToCart(mobileId)}>Add to cart</button>
                    </div>
                    <div className='mobilDetail_spec'>
                        <h2 className='mobilDetail_specBrand'>{descripcion.brand}</h2>
                        <h3 className='mobilDetail_specModel'>{descripcion.model}</h3>
                        <div>Price: {descripcion.price} €</div>
                        <div>CPU: {descripcion.cpu}</div>
                        <div>RAM: {descripcion.ram}</div>
                        <div>Operative system: {descripcion.os}</div>
                        <div>Display resolution: {descripcion.displayResolution}</div>
                        <div>Battery: {descripcion.battery}</div>
                        <div>Cams: (primary: {descripcion.primaryCamera}, secondary:{descripcion.secondaryCmera})</div>
                        <div>Dimentions: {descripcion.dimentions}</div>
                        <div>Weight: {descripcion.weight}</div>
                        <div onClick={()=>setShowAllSpecs(!showAllSpecs)}>{showAllSpecs ? '[-] Hide' : '[+] Show' } all specifications</div>
                        <div  className={showAllSpecs ? 'mobilDetail_specAll' : 'mobilDetail_specAll mobilDetail_specAll-hidden'}>
                            <ul>{hadleDescription(descripcion)}</ul>
                        </div>
                    </div>
                </div>
            )
        }
    }

    const renderMessage = () => {
        if (messageOK) {
            return (<div className='mobilDetail_messageOK'>{messageOK}</div>)
        }
    }

    return (
        <div className='mobilDetail'>
            {errorService !== '' ? <div className='mobilDetail_error'>{errorService}</div> :
                <div>
                    {renderMessage()}
                    {renderDetails(description)}
                </div>
            }
        </div>
    )
}

export default MobilDetail;
