import React, {useState, useEffect} from 'react';
import {Link} from "react-router-dom";
import Paginator from '../paginator/Paginator'
import {compareMinutes} from '../../utils/lib.js'
import {API_ALL_PRODUCT, ERROR_NO_SERVICE} from '../../utils/constants.js'

import './ListMobiles.css'

const maxView = 12
const maxMinutesStore = 60

const useStorageLocal = () => {
    const [allMobiles] = useState(() => {
        const saved = localStorage.getItem('allMobiles');
        const initialValue = JSON.parse(saved);
        return initialValue || "";
    })
    const setAllMobiles = (result) => {
        let resultAll = {time: new Date().getTime(), values: result}
        localStorage.setItem("allMobiles", JSON.stringify(resultAll))
    }
    return {allMobiles, setAllMobiles}
}

const ListMobiles = () =>{
    const [mobiles, setMobiles] = useState([])
    const [search, setSearch] = useState('')
    const [init, setInit]  = useState(0)
    const {allMobiles, setAllMobiles} = useStorageLocal()
    const [errorService, setErrorService] = useState('')

    const changeInputSearch = (e) => {
        setInit(0)
        setSearch(e.target.value)
    }

    const limitNumberView = () => {
        return mobiles.slice(init*maxView, (init*maxView)+maxView)
      }

    const presentMobiles = (listMobiles) => {
        let items = ''
        if (listMobiles.length > 0) {
            items = listMobiles.map((mobil) =>
                <div className='listMobiles_item'>
                    <Link to={'/mobile/' + mobil.id}>
                        <img src={mobil.imgUrl} alt={mobil.model} className='listMobiles_itemImage'/>
                        <div className='listMobiles_itemBrand'>{mobil.brand}</div>
                        <div className='listMobiles_itemModel'>{mobil.model}</div>
                        <div className='listMobiles_itemPrice'>{mobil.price} €</div>
                    </Link>
                </div>
            )
        }
        return items
    }

    const clickPaginator = (ini) => {
        setInit(ini)
    }

    useEffect(() => {
        if (search !== '') {
            let mobils = allMobiles.values.filter(item =>
                item.brand.toUpperCase().indexOf(search.toUpperCase()) !== -1 ||
                item.model.toUpperCase().indexOf(search.toUpperCase()) !== -1 ||
                item.price.toUpperCase().indexOf(search.toUpperCase()) !== -1)
            setMobiles(mobils)
        } else {
            setMobiles(allMobiles.values)
        }
      }, [search, allMobiles]);

    useEffect(() => {
        const getAllProducts = () => {
            fetch(API_ALL_PRODUCT)
            .then(res => res.json())
            .then(
                (result) => {
                    if (result !== null && result !== undefined) {
                        setAllMobiles(result)
                        setMobiles(result)
                    } else {
                        setErrorService(ERROR_NO_SERVICE)
                    }
                },
                (error) => {
                    console.log(error)
                    setErrorService(ERROR_NO_SERVICE)
                }
            )
        }
        if (allMobiles === '') {
            getAllProducts()
        } else {
            if (compareMinutes(allMobiles.time) >= maxMinutesStore) {
                debugger
                getAllProducts()
            }
        }
      }, [allMobiles]);

    const renderList = () => {
        if (mobiles !== undefined && mobiles.length > 0) {
            return (
                <div>
                    <div className='listMobiles_header'>
                        <h2 className='listMobiles_title'>Featured</h2>
                        <div className='listMobiles_input'>
                            <input name='' placeholder='Search...' onChange={(e)=>changeInputSearch(e)} value={search}/>
                        </div>
                    </div>
                    <div className='listMobiles_list'>
                        {presentMobiles(limitNumberView())}
                    </div>
                    <Paginator init={init} maxView={maxView} length={mobiles.length} click={clickPaginator}/>
                </div>
            )
        }
    }
    return (
        <div className='listMobiles'>
            {errorService !== '' ? <div className='listMobiles_error'>{errorService}</div> : renderList()}
        </div>
    )
}

export default ListMobiles;
