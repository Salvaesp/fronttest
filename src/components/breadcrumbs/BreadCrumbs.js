import React from "react";
import { useLocation, Link } from "react-router-dom";

import './BreadCrumbs.css'

const BreadCrumbs = () => {
  // Don't render a single breadcrumb.
  const location = useLocation()

  const presentBreadCrumb = () => {
    if (location.pathname !== '/') {
      return <span><Link to='/'>Home</Link> &gt; View Mobile</span>
    }
  }

  return (
    <div className='breadCrumbs'>{presentBreadCrumb()}</div>
  )

}

export default BreadCrumbs;
