import React, {useState} from 'react'
import {BrowserRouter, Routes, Route} from "react-router-dom"
import Header from './components/header/Header'

import {routes, CartContext} from './utils/constants.js'

import './App.css'

const App = () => {
	const [cart, setCart] = useState([])

	const addToCart = (product) => {
		setCart([...cart, product])
	}

	return (
		<div className='aplication'>
		<BrowserRouter>
			<CartContext.Provider value={{cart, addToCart}}>
				<Header />
				<Routes>
				{routes.map(({ path, element }) => (
					<Route exact path={path} element={element} />
					))}
				</Routes>
			</CartContext.Provider>
		</BrowserRouter>
		</div>
	)
}

export default App;
