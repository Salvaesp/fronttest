# frontTest

Tienda online de telefofía móvil. Se basa en una SPA con dis vistas: la primera donde se mostrará un listado de teléfonos disponibles y una segunda con la descripción de un telefóno seleccionado. En esta segunda vista se permetirá añadir el terminal al carrito de compra.

## Getting started

mkdir front-test-app
cd front-test-app
git clone https://gitlab.com/Salvaesp/fronttest.git . 
npm install
npm start

## notes

- En los requisitos se pedía que la busqueda fuese en directo, pero la API del listado de productos no permite el filtrado directo a través de parámetros, por lo que la busqueda se hace sobre todo el listado obtenido anteriormente
